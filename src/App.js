import './App.css';
import Header from "./components/Header";
import banner from './banner_image.png';
import facebook from './hung.png';
import youtube from './hung-1.png';
import lock from './banner_icon_4.png'
import atp from './atp.png'
import SideBar from "./components/SideBar";
import React, { useEffect, useState } from 'react';
import AOS from 'aos'
import "aos/dist/aos.css";
import axios from "axios";
import GlobalLoading from "./components/GlobalLoading";

function App() {
	const serverURL = "/api/getUID";
	const [values, setValues] = useState({
		url: "",
		type: "page",
	});

	const [loading, setLoading] = useState({
		showLoading: false,
	})
	const [data, setData] = useState({
		uid: "",
		type:"",
		message: ""
	})


	useEffect(() => {
		AOS.init({
			duration: 1200
		});
	}, [data, loading]);

	const handleChange = (event) => {
		if(event.target.name === "type" && event.target.value !== values.type){
			setData({
				uid: "",
				type: event.target.value,
				message: ""

			})
		}
		setValues({
			...values,
			[event.target.name]: event.target.value
		});
	};
	const onSubmit = () => {
		setLoading({
			showLoading: true
		})
		axios.post(serverURL, values)
			.then(function (res) {
				const uid = res.data.uid;
				const type = res.data.type;
				const message = res.data.message;

				setData({
					uid,
					type,
					message,
				})


				setLoading({
					showLoading: false
				})

			})
			.catch(function (error) {
				setLoading({
					showLoading: false
				})
				console.log(error);
			});


	}




	return (
		<div className="application application-offset ready">
			{ loading.showLoading === true && <GlobalLoading />}
			<div className="container-fluid container-application">
				<SideBar />
				<div className="main-content position-relative">
					<Header />
					<div className="page-content">
						<section className="xs-banner" style={{ width: '100%' }}>
							<div className="container">
								<div className="row">
									<div className="col-md-12 align-self-center mb-5">
										<div className="xs-banner-content ml-3">
											<h1 className="text-white font-weight-600 mb-5 mt-5 text-center" data-aos={"fade-right"} style={{ fontSize: '54px' }}>FIND UID</h1>
											<p className="text-white text-center" data-aos={"fade-up"}
											   style={{ fontSize: "20px" }}>Công cụ miễn phí giúp bạn dễ dàng lấy UID Facebook một cách dễ dàng.</p>
											<p className="text-white text-center text-break" data-aos={"fade-up"}
											   style={{ fontSize: "13px" }}>
												Bằng cách nhập dữ liệu trên hệ thống tìm kiếm của chúng tôi, bạn đã đồng ý với
												<b> Điều khoản sử dụng </b>
												và
												<b> Chính sách bảo mật</b> của chúng tôi.
											</p>
										</div>
									</div>
								</div>
								<div className="row d-flex justify-content-center">
									<div className="col-lg-8 col-md-12">
										<form className="omnisearch-form card">
											<div className="form-group mt-3 px-2 py-2">
												<select name="type"
														value={values.type}
														onChange={handleChange}
														className="custom-select">
													<option value='group'>Group</option>
													<option value='post'>Post</option>
													<option value='page'>Page</option>
													<option value='profile'>Profile</option>
												</select>
											</div>
											<div className="form-group" style={{ marginBottom: "0px" }}>
												<div className="input-group input-group-merge input-group-flush short-group">
													<div className="col-lg-10 col-md-12 d-flex merge-group">
														<div className="input-group-prepend">
															<span className="input-group-text" style={{ borderRadius: "40px" }}><i className="fas fa-link"></i></span>
														</div>
														<input
															type="text"
															name="url"
															className="form-control link-input"
															placeholder="Nhập link muốn tìm kiếm..."
															onChange={handleChange}
															value={values.url}
															style={{ color: 'black' }}
														/>
													</div>
													<div className="group-button col-lg-2 col-md-12 d-flex justify-content-end">
														<a
															className="btn btn-md btn-primary rounded-pill btn-icon"
															onClick={() => onSubmit()}
														>
															<i className="fas fa-search"></i>
														</a>
													</div>
												</div>
											</div>
											<div className="card-footer" style={{ padding: "5px" }}></div>
										</form>
										{(data.message === "successfully") && (<>
											<div className="card d-flex flex-column align-items-center ">
												<p className="h4 mt-3">UID Bạn Muốn Tìm Là:</p>
												<p className="h5" style={{ color: "#ff5630" }}>{data.uid}</p>
											</div></>)
										}
										{(data.message === "failed") && (<>
											<div className="card d-flex flex-column align-items-center ">
												<p className="h5 mt-3" style={{ color: "#ff5630" }}>Xin lỗi hệ thống không tim được UID của bạn, hãy chắc chắn rằng URL của bạn hợp lệ</p>
											</div></>)
										}
										<div>
											<h5 className="text-white">Đường dẫn hợp lệ</h5>
											{values.type === 'profile' && <p className="text-white text-break">https://www.facebook.com/xxxxxxxxxx</p>}
											{values.type === 'group' && <p className="text-white text-break">https://www.facebook.com/groups/xxxxxxxxxx</p>}
											{values.type === 'page' && <p className="text-white text-break">https://www.facebook.com/xxxxxxxxxx</p>}
											{values.type === 'post' && <>
												<p className="text-white text-break">https://www.facebook.com/post/xxxxxxxxxx</p>
												<p className="text-white text-break">https://www.facebook.com/photo?fbid=xxxxxxxxxx</p>
												<p className="text-white text-break">https://www.facebook.com/user_name_or_page/posts/xxxxxxxxxx</p>
												<p className="text-white text-break">https://www.facebook.com/user_name_or_page/videos/xxxxxxxxxx</p>
												<p className="text-white text-break">https://www.facebook.com/user_name_or_page/posts/xxxxxxxxxx</p>
												<p className="text-white text-break">https://www.facebook.com/permalink.php?story_fbid=xxxxxxxxxx&id=xxxxxxxxxx</p>
											</>}
										</div>
									</div>
								</div>
							</div>
						</section>
						<hr />
						<section className="xs-section-padding xs-hosting-info">
							<div className="container">
								<div className="row mb-5">
									<div className="col-lg-4 col-md-6">
										<div className="hosting-info-img">
											<img className="image-bg" src="/asset/img/sum_link_image/image-0.png"
												 alt="Hosting info image" draggable="false" />
										</div>
									</div>
									<div className="col-lg-8 align-items-center justify-content-center d-flex">
										<div className="hosting-info-wraper">
											<div className="spinner-icon wow animated text-center">
												<img src="/asset/img/sum_link_image/electric-wave.png"
													 alt="hosting info info" draggable="false" />
											</div>
											<div data-aos={"fade-up"}>
												<h2 className="content-title text-center">TÌM KIẾM NICK FACEBOOK DỄ DÀNG</h2>
												<ul className="xs-list check" style={{ width: "60%", margin: "auto" }}>
													<li className="text-justify">Như các bạn đã biết, Facebook đã chặn tìm kiếm nick facebook theo số điện thoại, nhưng với findUID bạn hoàn toàn dễ dàng có thể kiểm tra SĐT có đăng ký Facebook hay không mà không lo Facebook chặn</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<hr />
								<div className="row mb-5">
									<div className="col-lg-8 align-self-center">
										<div className="hosting-info-wraper-2 " data-aos={"fade-right"} data-aos-duration="1500">
											<h2 className="content-title mb-5">GIAO DIỆN TRỰC QUAN DỄ SỬ DỤNG</h2>
											<div className="media hosting-info-list d-flex align-items-center">
												<div className="media-body">
													<ul className="xs-list check" style={{ width: "60%" }}>
														<li className="text-justify">Chỉ với vài thao tác đơn giản bạn có thể tìm tài khoản Facebook bạn bè, khách hàng, đối tác,… để kết nối trên Facebook</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<div className="col-lg-4 col-md-6">
										<div className="hosting-info-img" data-aos={"fade-left"} data-aos-duration="2000">
											<img className="image-bg" src="/asset/img/sum_link_image/image-1.png"
												 alt="Hosting info image" draggable="false" />
										</div>
									</div>
								</div>
								<hr />
								<div className="row">
									<div className="col-lg-4 col-md-6">
										<div className="hosting-info-img">
											<img className="image-bg" src="/asset/img/sum_link_image/image-2.png"
												 alt="Hosting info image" draggable="false" />
										</div>
									</div>
									<div className="col-lg-8 align-items-center justify-content-center d-flex">
										<div className="hosting-info-wraper">
											<div className="spinner-icon wow animated text-center">
												<img src="/asset/img/sum_link_image/electric-wave.png"
													 alt="hosting info info" draggable="false" />
											</div>
											<div data-aos={"fade-up"}>
												<h2 className="content-title text-center">TIẾP CẬN DỄ DÀNG HƠN VỚI FINDUID</h2>
												<ul className="xs-list check" style={{ width: "60%", margin: "auto" }}>
													<li className="text-justify">Bạn không còn kho khăn trong việc tìm kiếm khách hàng trên Faceook nữa, với FindUID của chúng tôi hỗ trợ đăc lực cho những anh em làm bất động sản, telesale....</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						<hr />
					</div>
					<div className="footer pt-5 pb-4 footer-light" id="footer-main">
						<div className="row text-center text-sm-left align-items-sm-center">
							<div className="col-sm-6">
								<p className="text-sm mb-0">© 2019 <a href="https://webpixels.io" className="h6 text-sm"
																	  target="_blank">Webpixels</a>. All rights
									reserved.</p>
							</div>
							<div className="col-sm-6 mb-md-0">
								<ul className="nav justify-content-center justify-content-md-end">
									<li className="nav-item dropdown border-right">
										<a className="nav-link" href="#" role="button" data-toggle="dropdown"
										   aria-haspopup="true" aria-expanded="false">
											<img alt="Image placeholder" src="/asset/img/icons/flags/ro.svg" />
											<span className="h6 text-sm mb-0">RO</span>
										</a>
										<div className="dropdown-menu dropdown-menu-sm dropdown-menu-right">
											<a href="#" className="dropdown-item"><img alt="Image placeholder"
																					   src="/asset/img/icons/flags/es.svg"
																					   className="mr-2" />Spanish</a>
											<a href="#" className="dropdown-item"><img alt="Image placeholder"
																					   src="/asset/img/icons/flags/us.svg"
																					   className="mr-2" />English</a>
											<a href="#" className="dropdown-item"><img alt="Image placeholder"
																					   src="/asset/img/icons/flags/gr.svg"
																					   className="mr-2" />Greek</a>
										</div>
									</li>
									<li className="nav-item">
										<a className="nav-link" href="#">Support</a>
									</li>
									<li className="nav-item">
										<a className="nav-link" href="#">Terms</a>
									</li>
									<li className="nav-item">
										<a className="nav-link pr-0" href="#">Privacy</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}

export default App;
