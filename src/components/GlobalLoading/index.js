import React from 'react';
import loading from '../../purple-loading.gif';
import './globalLoading.css'

function GlobalLoading(){


    return(
        <div className="global-loading">
            <img src={loading} alt="loading" className="loading-image"/>
        </div>
    );
}

export default GlobalLoading