import React from 'react';
import logo from '../logo.png';
import avatar from '../../src/avatar.png';

function SideBar(){


    return(
        <div className="sidenav" id="sidenav-main">
            <div className="sidenav-header d-flex align-items-center">
                <a className="navbar-brand" to="/app/add-frame-page"> <img src="/asset/img/brand/trang.png"
                                                                       className="navbar-brand-img" alt="..."
                                                                       width="100"/>
                </a>
                <div className="ml-auto">
                    <div className="sidenav-toggler sidenav-toggler-dark d-md-none" data-action="sidenav-unpin"
                         data-target="#sidenav-main">
                        <div className="sidenav-toggler-inner">
                            <i className="sidenav-toggler-line bg-white"></i> <i
                            className="sidenav-toggler-line bg-white"></i> <i
                            className="sidenav-toggler-line bg-white"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SideBar