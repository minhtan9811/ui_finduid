import React from 'react'
import logo from '../logo.png';

function Header(){
    return(
        <nav className="navbar navbar-main navbar-expand-lg navbar-dark bg-primary navbar-border"
             id="navbar-main">
            <div className="container-fluid">
                <button className="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbar-main-collapse"
                        aria-controls="navbar-main-collapse" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="navbar-user d-lg-none ml-auto">
                    <ul className="navbar-nav flex-row align-items-center">
                    </ul>
                </div>
                <div className="collapse navbar-collapse navbar-collapse-fade" id="navbar-main-collapse">
                    <ul className="navbar-nav align-items-lg-center">
                        <li className="nav-item active">
                            <a href="#" className="nav-link pl-lg-0" to="/app/add-frame-page"> Home </a>
                        </li>
                    </ul>
                    <ul className="navbar-nav ml-lg-auto align-items-center d-none d-lg-flex">
                        <li className="nav-item">
                            <a href="#" className="nav-link nav-link-icon sidenav-toggler"
                               data-action="sidenav-pin"
                               data-target="#sidenav-main"><i className="fas fa-bars"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    );
}

export default Header